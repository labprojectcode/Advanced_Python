
a = lambda a:a+2
print(a(5))

b = lambda x:x%2==0
print(b(120),"=>Even no")

c = lambda x,y: x+y > y-x 
print(c(10,15))

d = lambda x,y,z : (x and y > z) and (y and z >x) and (z and x > y)
print(d(10,15,20))

e = lambda v1,v2 : (v1%v2==0) or (v2%v1==0) or (v1==v2==0)
print(e(100,-200))
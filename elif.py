# Password complexity Check
#=============================

import re
password = input("Enter Your Password :")
ch = True
while(ch):
    
    if len(password)<6 or len(password)>16:
        print("password is invalid")
        break
    
    elif not re.search('[A-Z]',password):
        print("password is invalid")
        break
    
    elif not re.search('[a-z]',password):
        print("password is invalid")
        break
    
    elif not re.search('[0-9]',password):
        print("password is invalid")
        break
    
    elif not re.search('[@#$%]',password):
        print("password is invalid")
        break
    
    else:
        print("!! Valid Password !!")
        break 

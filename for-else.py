#Using For else section
#========================


from re import I


def func1():
    for i in range(1,n):
        if i==n:
            print(i)
    else:
        print("else part of for loop",(i))
        
    print("hello I")
        
        
if __name__=='__main__':
    n=10
    func1()
    
    
    
no_of_items = [2,18,86,71,44]
divisor = 12

for i in no_of_items:
    if i % divisor == 0:
        found = i 
        break
else:
    no_of_items.append(divisor)
    found = divisor
    
print("{i} contains {found} which is a multiple of {divisor}".format(**locals()))
    
    
    
 
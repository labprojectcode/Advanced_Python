# Using while loop with else part
#==========================================

from email.policy import default
from math import pi
import sys
from turtle import goto

from pip import main 

class Rectangle:
        
    def __init__(self,width,length):
        self.width = width
        self.length = length 
        
    def areaRectangle(self):
        area_of_rect = (self.width * self.length)
        print(f"Area of Rectangle : {area_of_rect}")
        
    def perimeterRectangle(self):
        peri_of_rect = 2*(self.width + self.length)
        print(f"Perimeter of Rectangle : {peri_of_rect}")
        
        
class Circle:
    
    def __init__(self,r):
        self.r = r
        
    def areaCircle(self):
        area_of_circle = (pi * self.r *self.r)
        print(f"Area of Circle : {area_of_circle}")
        
    def perimeterCircle(self):
        peri_of_circle = (2 * pi *self.r)
        print(f"Perimeter of Circle : {peri_of_circle}")
        
        
if __name__=='__main__':
    
    msg = '''
           1. Area of Rectangle
           2. Area of Circle
           3. Perimeter of Rectangle
           4. Perimeter of Circle
           5. Exit from program
    '''
    ch = 0
    while(ch!=5):
        print(msg)
        ch = int(input("enter your choice :"))
        if ch==1:
            w = float(input("Enter width :"))
            l = float(input("Enter length :"))
            r1 = Rectangle(w,l)
            r1.areaRectangle()  
        
        if ch==2:
            radius = float(input("Enter radius :"))
            c1 = Circle(radius)
            c1.areaCircle()
        
        if ch==3:
            w = float(input("Enter width :"))
            l = float(input("Enter length :"))
            r1 = Rectangle(w,l)
            r1.perimeterRectangle()
        
        if ch==4:
            radius = float(input("Enter radius :"))
            c1 = Circle(radius)
            c1.perimeterCircle()
        
        if ch==5:
            con = input("Are you want to continue ? (y/n)")
            if con=="y":
                print(msg)
                ch = int(input("enter your choice :"))
            else:
                sys.exit(1)
        
else:
    print("condition does not match !!")
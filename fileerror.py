#file error found with try-else block
'''
try:
    filename = input("Enter file name :")
    content = input("enter content :")
    fw = open(filename,"w")
    fw.write(content) 
       
except FileNotFoundError:
    print("!! File does not exist !!")
    
else:
    fw.close()
''' 
    
try:
    filename = input("Enter file name :")
    with open(filename,"r") as fr:
            print(fr.read())
            
except FileNotFoundError:
    print("!! File does not exist !!")
    
else:
    fr.close()

    